package common;

import config.ConfigPropertyNames;
import io.restassured.RestAssured;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import lombok.Getter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.Properties;

public abstract class BaseTestConfig {

    @Getter
    private static final Properties prop;

    static {

        System.out.println("!!!!!!!!!!!!!!!!   Reading .properties  file    !!!!!!!!!!!!!!!!!!");
        prop = new Properties();
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(
                        Thread.currentThread().getContextClassLoader()
                                .getResourceAsStream("env.properties")), StandardCharsets.UTF_8))) {
            prop.load(reader);

        } catch (IOException e) {
            e.printStackTrace();
        }


        RestAssured.baseURI = prop.getProperty(ConfigPropertyNames.BASE_URI.prop());
        RestAssured.port = Integer.parseInt(prop.getProperty(ConfigPropertyNames.PORT.prop()));
        RestAssured.basePath = prop.getProperty(ConfigPropertyNames.BASE_PATH.prop());
        RestAssured.filters(Arrays.asList(new RequestLoggingFilter(), new ResponseLoggingFilter(), new ErrorLoggingFilter()));
    }
}
