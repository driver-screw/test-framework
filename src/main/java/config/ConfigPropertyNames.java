package config;

public enum ConfigPropertyNames {
    BASE_URI("baseURI"),
    PORT("port"),
    BASE_PATH("basePath");


    String propName;

    ConfigPropertyNames(String propName) {
        this.propName = propName;
    }

    public String prop() {
        return propName;
    }
}
