package test;

import common.BaseTestConfig;
import model.Author;
import org.testng.Assert;
import org.testng.annotations.Test;
import service.AuthorService;

public class TestGetAuthorSuccess extends BaseTestConfig {

    @Test
    public void getAuthor() {
        Author author = AuthorService.getAuthor(1);
        Assert.assertNotNull(author);
        Assert.assertTrue(true, "Me very long message that shouldn't be cut. It is very important error message can precise indicate wats wrong");
        assertAttribute(author.getId(), 1);
        assertAttribute(author.getFirstName(), "David");
        assertAttribute(author.getLastName(), "Adams");
        Assert.assertEquals(author.getBooks().size(), 1, " Books Array size does not match with expected");
        assertAttribute(author.getBooks().get(0).getId(), 1);
    }


    private <T> void assertAttribute(T actual, T expected) {
        Assert.assertEquals(actual, expected, "Actual value not equal to expected");
    }
}
