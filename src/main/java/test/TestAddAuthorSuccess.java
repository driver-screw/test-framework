package test;

import common.BaseTestConfig;
import model.Author;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import service.AuthorService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestAddAuthorSuccess extends BaseTestConfig {

    private List<Integer> authorIdToDelete = new ArrayList<>();

    @Test
    public void addAuthor() {
        var authorToAdd = new Author();
        authorToAdd.setFirstName("Alan");
        authorToAdd.setLastName("Wake");
        var author = AuthorService.addAuthor(authorToAdd);
        authorIdToDelete.add(author.getId());
        compareAuthors(author, authorToAdd);


        var authorGetById = AuthorService.getAuthor(author.getId());
        compareAuthors(authorGetById, authorToAdd);
    }

    @Test(dataProvider = "AuthorName")
    public void addAuthorDataProvider(String firstName, String lastName, String middleName) {
        var authorToAdd = new Author();
        authorToAdd.setFirstName(firstName);
        authorToAdd.setLastName(lastName);
        authorToAdd.setMiddleName(middleName);
        var author = AuthorService.addAuthor(authorToAdd);
        authorIdToDelete.add(author.getId());
        compareAuthors(author, authorToAdd);

        var authorGetById = AuthorService.getAuthor(author.getId());
        compareAuthors(authorGetById, authorToAdd);
    }

    @AfterClass
    public void cleanUp() {
        authorIdToDelete.forEach(AuthorService::deleteAuthor);
    }

    @DataProvider(name = "AuthorName")
    public Iterator<Object[]> createData1() {
        var string45 = "45SymbolString1234567894561231234560123456789";
        List<Object[]> list = new ArrayList<>();
        list.add(new Object[]{"Alan", "Wake", "Couldronlakovich"});
        list.add(new Object[]{"", "", ""});
        list.add(new Object[]{"A", "A", "A"});
        list.add(new Object[]{"Алан", "Вейко", "Колдронлейкович"});
        list.add(new Object[]{string45, string45, string45});
        return list.iterator();
    }


    private void compareAuthors(Author actualAuthor, Author expectedAuthor) {
        Assert.assertEquals(actualAuthor.getFirstName(), expectedAuthor.getFirstName());
        Assert.assertEquals(actualAuthor.getLastName(), expectedAuthor.getLastName());
    }
}
