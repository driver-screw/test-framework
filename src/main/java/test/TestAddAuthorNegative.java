package test;

import common.BaseTestConfig;
import model.Author;
import model.error.GeneralErrorResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import service.AuthorService;

public class TestAddAuthorNegative extends BaseTestConfig {

    GeneralErrorResponse errorResponse = new GeneralErrorResponse(400, "could not execute statement", 0);

    @Test
    public void testAddAuthorWithoutLastNameField() {
        var authorToAdd = new Author();
        authorToAdd.setFirstName("Alan");
        AuthorService.addAuthor(authorToAdd, HttpStatus.SC_BAD_REQUEST, errorResponse);
    }

    @Test
    public void testAddAuthorWithoutFirstNameField() {
        var authorToAdd = new Author();
        authorToAdd.setLastName("Wake");
        AuthorService.addAuthor(authorToAdd, HttpStatus.SC_BAD_REQUEST, errorResponse);
    }

    @Test
    public void testAddAuthorWithEmptyRequest() {
        var authorToAdd = new Author();
        AuthorService.addAuthor(authorToAdd, HttpStatus.SC_BAD_REQUEST, errorResponse);
    }


}
