package service;

import io.restassured.http.ContentType;
import io.restassured.http.Header;
import model.Author;
import model.error.GeneralErrorResponse;
import org.apache.http.HttpStatus;
import org.testng.Assert;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

public class AuthorService {

    private AuthorService() {
    }

    public static Author getAuthor(int id) {

        return when().get("authors/" + id)
                .then().assertThat().statusCode(HttpStatus.SC_OK).contentType(ContentType.JSON)
                .extract().response().as(Author.class);
    }

    public static Author addAuthor(Author author) {

        return given().body(author).header(new Header("Content-Type", "application/json"))
                .when().post("authors")
                .then().assertThat().statusCode(HttpStatus.SC_CREATED).contentType(ContentType.JSON)
                .extract().response().as(Author.class);
    }

    public static void addAuthor(Author author, int httpStatus, GeneralErrorResponse expectedErrorResponse) {

        GeneralErrorResponse actualResponse = given().body(author).header(new Header("Content-Type", "application/json"))
                .when().post("authors")
                .then().assertThat().statusCode(httpStatus).contentType(ContentType.JSON)
                .extract().response().as(GeneralErrorResponse.class);
        Assert.assertEquals(actualResponse.getStatus(), expectedErrorResponse.getStatus());
        Assert.assertEquals(actualResponse.getMessage(), expectedErrorResponse.getMessage());
    }

    public static void deleteAuthor(int id) {
        given().header(new Header("Content-Type", "application/json"))
                .when().delete("authors/" + id)
                .then().assertThat().statusCode(HttpStatus.SC_NO_CONTENT);
    }
}
