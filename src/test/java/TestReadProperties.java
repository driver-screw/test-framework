import common.BaseTestConfig;
import config.ConfigPropertyNames;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestReadProperties extends BaseTestConfig {

    @Test
    public void testReadProperties() {
        String propertyValue = getProp().getProperty(ConfigPropertyNames.BASE_URI.prop());
        Assert.assertEquals(propertyValue, "http://localhost");
    }
}
